CXXFLAGS= -Wall

all: executable

debug: CXXFLAGS+= -DDEBUG -g
debug: executable

executable: main.o
	$(CXX) $(CXXFLAGS) -o program main.o

main.o: main.cpp
	$(CXX) $(CXXFLAGS) -c main.cpp

clean:
	rm -f *.o program
