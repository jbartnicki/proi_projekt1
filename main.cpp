/* Jan Bartnicki 2I2
 * Zad1
 * Program służy do pobrania od użytkownika imienia, nazwiska i pseudonimu,
 * a następnie wypisania wszystkich razem, ze wszystkimi głoskami zamienionymi na 'z' oraz z
 * odwróconą kolejnością liter. */

#include <iostream>
#include <cctype>
#include <algorithm>

using namespace std;

bool isVowel(char ch); // mówi czy dany znak reprezentuje samogłoskę
void vowelToZ(string& str); // zamienia samogłoski na znak 'z'
void invertString(string& str); // odwraca kolejność liter

int main()
{
  string name, surname, pseudonym;

  cout << "Write name:\n";
  cin >> name;
  invertString(name);

  cout << "Write surname:\n";
  cin >> surname;
  invertString(surname);

  cout << "Write pseudonym:\n";
  cin >> pseudonym;
  invertString(pseudonym);
  
  // Zamień wszystkie samogłoski na 'z'
  vowelToZ(name); vowelToZ(surname); vowelToZ(pseudonym);

  cout << name << " " << surname << " \"" << pseudonym << "\"\n";

  return 0;
}

bool isVowel(char ch)
{
  ch = tolower(ch);
  return ch == 'a' || ch == 'e' || ch == 'i' ||
         ch == 'o' || ch == 'u' || ch == 'y';
}

void vowelToZ(string& str)
{
  int len = str.length();
  for(int i = 0; i < len; i++) {
    if(isVowel(str[i])) {
      str[i] = isupper(str[i]) ? 'Z' : 'z';
    }
  }
}

void invertString(string& str)
{
  int last = str.length() - 1; // indeks ostatniego znaku, użyty przy iterowaniu
  int len = str.length() / 2;

  for(int i = 0; i < len; i++) {
    swap(str[i], str[last - i]);
  }
}
