## Opis

Program pobiera od użytkownika *imię*, *nazwisko* i *pseudonim* a następnie wypisuje je ze wszystkimi samogłoskami zamienionymi na **'z'** oraz z **odwróconą** kolejnością liter.
